<?php

namespace App\Controller;

use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\AddProductType;

class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product")
     */
    public function index()
    {
        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
        ]);
    }
    /**
     * @Route("/annonce/ajouter", name="product_add")
     */
    public function addProduct()
    {
        $product = new Product();
        $form = $this->createForm(AddProductType::class, $product);
        return $this->render('product/add.html.twig', [
            'controller_name' => 'ProductController',
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/add/subcategory", name="subCategory_add")
     */
    /*public function addSubCategoryAction() {

        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        if ($request->isXmlHttpRequest()) { // pour vérifier la présence d'une requete Ajax
            $id = '';
            $id = $request->get('id');
            if ($id != '') {
                $ensembles = $em->getRepository('Category')
                    ->getSousCategory($id);
                $tabEnsembles = array();
                $i = 0;
                foreach ($ensembles as $ensemble) { // transformer la réponse à ta requete en tableau qui replira le select pour ensembles
                    $tabEnsembles[$i]['id'] = $ensemble->getId();
                    $tabEnsembles[$i]['title'] = $ensemble->getTitle();
                    $i++;
                }
                $response = new Response();
                $data = json_encode($tabEnsembles); // formater le résultat de la requête en json
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;
            }
        }
    }*/
}
