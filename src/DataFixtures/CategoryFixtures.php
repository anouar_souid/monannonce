<?php

namespace App\DataFixtures;
use App\Entity\SubCategory;
use Faker;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    protected $faker;
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        $categories= array('EMPLOI', 'VÉHICULES', 'IMMOBILIER', 'VACANCES', 'LOISIRS', 'MODE', 'MULTIMÉDIA', 'MAISON', 'MATÉRIEL PROFESSIONNEL', 'DIVERS');
        foreach ($categories as $cat){
            $category = new Category();
            $category->setName($cat);
            $manager->persist($category);
        }
        $manager->flush();
    }
}
