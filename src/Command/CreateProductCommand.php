<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateProductCommand extends Command
{
// the name of the command (the part after "bin/console")
protected static $defaultName = 'add:product';

protected function configure()
{
    $this
        // the short description shown while running "php bin/console list"
        ->setDescription('Creates a new product.')

        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp('This command allows you to create a product...')
    ;
}

protected function execute(InputInterface $input, OutputInterface $output)
{
    $output->writeln([
        'product Creator',
        '============',
        '',
    ]);

}
}